<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Whoami</title>
  </head>
  <body>
	<!-- navbar  -->
    <nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Whoami<small><sup>BETA</sup></small></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				</ul>
			</div>
		</div>
	</nav>
		<!-- form -->
		<div class="container">
			<div class="row">
				
			  	<div class="col-sm-4">
                    <?php
        				$nome = $_GET["nome"];
                        echo "<strong>Nome:</strong> $nome<br>";
                        $cognome = $_GET["cognome"];
                        echo "<strong>Cognome:</strong> $cognome<br>";
                        $luogo = $_GET["luogo"];
                        echo "<strong>Luogo:</strong> $luogo<br>";
                        $compleanno = $_GET["compleanno"];
                        echo "<strong>Compleanno:</strong> $compleanno<br>";
                        $bio = $_GET["bio"];
                        echo "<strong>Bio:</strong> $bio";
        			?>
			  	</div>
			</div>
	    </div>
  </body>
</html>
