 <html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Whoami</title>
  </head>
  <body>
	<!-- navbar  -->
    <nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="new.php">Whoami<small><sup>BETA</sup></small></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				</ul>
			</div>
		</div>
	</nav>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
		<p>
			<?php
				$nome = $_GET["nome"];
                echo "Nome: $nome<br>";
                $cognome = $_GET["cognome"];
                echo "Cognome: $cognome<br>";
                $luogo = $_GET["luogo"];
                echo "Luogo $luogo<br>";
                $compleanno = $_GET["compleanno"];
                echo "compleanno: $compleanno<br>";
                $bio = $_GET["bio"];
                echo "Bio: $bio<br>";

                // conversione link attuale a link di visualizzazione
                $actuallink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $viewLink = str_replace("process.php","show.php",$actuallink);

                // generazione link

                require_once('googl-php/Googl.class.php');

                $googl = new Googl('AIzaSyAYeojuWzGjXwjAbfJpyfsbdkzLUOAThZk');

                $shortLink = $googl->shorten("$viewLink");

			?>
            <div class="alert alert-success">
                <strong> Ecco il tuo link:</strong><br><?php  echo "<a href=\"$shortLink\">$shortLink</a>";?>
            </div>
		</p>
      </div>
    </div>
  </div>
  </body>
</html>
