<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Whoami</title>
  </head>
  <body>
	<!-- navbar  -->
    <nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Whoami<small><sup>BETA</sup></small></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				</ul>
			</div>
		</div>
	</nav>
		<!-- form -->
		<div class="container">
			<div class="row">
			  	<div class="col-sm-6 col-sm-offset-3">
				  	<form action="process.php" method="get">
		  				<div class="form-group">
				  			<label for="nome">Nome: </label>
				  			<input type="text" class="form-control" name="nome" id="nome" placeholder="Mario">
                        </div>
                        <div class="form-group">
                            <label for="cognome">Cognome: </label>
				  			<input type="text" class="form-control" name="cognome" id="cognome" placeholder="Rossi">
                        </div>
                        <div class="form-group">
                            <label for="luogo">Vive a: </label>
				  			<input type="text" class="form-control" name="luogo" id="luogo" placeholder="Udine">

                        </div>
                        <div class="form-group">
                            <label for="compleanno">Nato il: </label>
                            <input type="date" class="form-control" name="compleanno" id="compleanno">
                        </div>
                        <div class="form-group">
                            <label for="bio">Bio: </label>
				  			<textarea cols="12" rows="6" class="form-control" name="bio" id="bio" placeholder="Ho un cavallo bianco"></textarea>
                        </div>
						<button type="submit" name="invia" class="btn btn-default">Invia</button>
		  			</form>
			  	</div>
			</div>
	    </div>
  </body>
</html>
